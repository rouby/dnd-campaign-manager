import { Auth, Hub } from 'aws-amplify';
import { CognitoUser } from '@aws-amplify/auth';
import * as React from 'react';

export default function useUser() {
  const [user, setUser] = React.useState<CognitoUser>();

  React.useEffect(() => {
    let mounted = true;

    Hub.listen('auth', onAuth);

    Auth.currentAuthenticatedUser()
      .then(user => mounted && setUser(user))
      .catch(() => {});

    return () => {
      mounted = false;
      Hub.remove('auth', onAuth);
    };

    function onAuth({
      payload: { event, data },
    }: import('@aws-amplify/core/lib-esm/Hub').HubCapsule) {
      switch (event) {
        case 'signIn':
          setUser(data);
          break;
        case 'signOut':
          setUser(undefined);
          break;
      }
    }
  });

  return user;
}
