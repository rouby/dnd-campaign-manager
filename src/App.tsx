import {
  CognitoHostedUIIdentityProvider,
  default as Auth,
} from '@aws-amplify/auth';
import { Button } from 'antd';
import React from 'react';
import { useUser } from './hooks';

export default function App() {
  const user = useUser();

  return (
    <div>
      {!user && (
        <div>
          <Button
            onClick={() =>
              Auth.federatedSignIn({
                provider: CognitoHostedUIIdentityProvider.Google,
              })
            }
          >
            Open Google
          </Button>
          <Button onClick={() => Auth.federatedSignIn()}>Open Hosted UI</Button>
        </div>
      )}
      {user && <Button onClick={() => Auth.signOut()}>Sign Out</Button>}
    </div>
  );
}
